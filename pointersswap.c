#include <stdio.h>
void pswap(int *m,int *n);
int main()
{
    int x,y,*m,*n;
    printf("Enter any two numbers\n");
    scanf("%d%d",&x,&y);
    m=&x;
    n=&y;
    printf("Before swapping the numbers x=%d y=%d\n",x,y);
    pswap(m,n);
    printf("After swapping the numbers x=%d y=%d\n",x,y);
    return 0;
}
void pswap(int *m,int *n)
{
    int p;
    p=*m;
    *m=*n;
    *n=p;
}